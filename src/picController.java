import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Formatter;
import java.util.List;
import java.util.Optional;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.input.DragEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.scene.input.TransferMode;
import javax.imageio.ImageIO;

public class picController {
	
    	@FXML
    	private AnchorPane ancpane;

	 	@FXML
	    private ImageView img1;

	    @FXML
	    private ImageView img2;

	    @FXML
	    private ImageView img3;  
	    
	    @FXML
	    private ImageView img4;

	    @FXML
	    private Button btnpic1;

	    @FXML
	    private Button btnpic2;

	    @FXML
	    private Button btnpic3;

	    @FXML
	    private Button btngenerate;

	    @FXML
	    private ImageView genimg;

	    @FXML
	    private TextField pic1url;

	    @FXML
	    private TextField pic3url;

	    @FXML
	    private TextField pic2url;

	    @FXML
	    private Button btnupload1;

	    @FXML
	    private Button btnupload2;

	    @FXML
	    private Button btnupload3;
	    
	    @FXML
	    private MenuBar prefmenu;
	    
	    @FXML
	    private TextArea errorfield;
	    
	    @FXML
	    private Button btnpreview; 
	    
	    @FXML
	    private TextField  borderbreddefelt;

	    private String filename = "test"; 
	    private String filetype = "png"; 
	    private String defaulturl = "http://www.adbazar.pk/frontend/images/default-image.jpg";
	    // border size 
	    Integer border;
	    
    @FXML
    public void initialize() {
    	/* init all variables */
    	
    	img1.setImage((new Image(defaulturl)));
    	img2.setImage((new Image(defaulturl)));
    	img3.setImage((new Image(defaulturl)));
    	//img4.setImage((new Image(defaulturl)));
    	border = 10; 

    	Image logo = new Image("logo.jpg");
    	img4.setImage(logo);
    	
    	errorfield.setText("0 error on startup");
        

    }
    
    @FXML
    void setpic1(ActionEvent event) throws MalformedURLException {
    	//Trying to load pictures via filechoser, thows exeption if unable 
    	try {
    		JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		
    		int returnValue = jfc.showOpenDialog(null);
		
		File selectedFile = null;
	
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			 selectedFile = jfc.getSelectedFile();
		}
		Image imageForFile = new Image(selectedFile.toURI().toURL().toExternalForm());

    	img1.setImage(imageForFile);

			
		} catch (MalformedURLException e) {
			errorfield.setText("Unable to load picture 1");
			// TODO: handle exception
		}
    	
    }
    
    @FXML
    void setpic2(ActionEvent event) throws MalformedURLException {
    	//Trying to load pictures via filechoser, thows exeption if unable 
    	try {
	
    	JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
	
    	int returnValue = jfc.showOpenDialog(null);
	
    	File selectedFile = null;
	
    	if (returnValue == JFileChooser.APPROVE_OPTION) {
			 selectedFile = jfc.getSelectedFile();
		}
	
    	Image imageForFile = new Image(selectedFile.toURI().toURL().toExternalForm());

    	img2.setImage(imageForFile);

			
		} catch (Exception e) {
			errorfield.setText("Unable to load picture 2");

		}
    	
    }
    
    @FXML
    void setpic3(ActionEvent event) throws MalformedURLException {
       	//Trying to load pictures via filechoser, thows exeption if unable 

    	try {
    		JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView());
    		int returnValue = jfc.showOpenDialog(null);
    		File selectedFile = null;
    		
    		
    		if (returnValue == JFileChooser.APPROVE_OPTION) {
    			 selectedFile = jfc.getSelectedFile();
    		}
    		Image imageForFile = new Image(selectedFile.toURI().toURL().toExternalForm());

        	img3.setImage(imageForFile);

		} catch (Exception e) {
			errorfield.setText("Unable to load picture 3");
		}
    
    }
    
    @FXML
    void setpic4(ActionEvent event) throws MalformedURLException {
       	//Trying to load pictures via filechoser, thows exeption if unable 

    	try {
    		JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView());
    		int returnValue = jfc.showOpenDialog(null);
    		File selectedFile = null;
    		
    		
    		if (returnValue == JFileChooser.APPROVE_OPTION) {
    			 selectedFile = jfc.getSelectedFile();
    		}
    		Image imageForFile = new Image(selectedFile.toURI().toURL().toExternalForm());

        	img4.setImage(imageForFile);

		} catch (Exception e) {
			errorfield.setText("Unable to load picture 4");
		}
    
    }
    
    @FXML
    void upload1(ActionEvent event) throws IllegalArgumentException {
       	//Trying to load pictures via URL, thows exeption if unable 

    	String url = pic1url.getText();
    	try {
        	img1.setImage(new Image(url));

		} catch (Exception e) {
			img1.setImage(new Image(defaulturl));
			pic1url.setText("Error: Invalid picture url");
			errorfield.setText("Error: Invalid picture url");

		}	
    }

    @FXML
    void upload2(ActionEvent event) throws IllegalArgumentException {
    	//Trying to load pictures via URL, thows exeption if unable 
    	String url = pic2url.getText();
    	try {
        	img2.setImage(new Image(url));

		} catch (Exception e) {
			img2.setImage(new Image(defaulturl));
			pic2url.setText("Error: Invalid picture url");
			errorfield.setText("Error: Invalid picture url");

		}
    	
    	
    	
    }

    @FXML
    void upload3(ActionEvent event)  throws IllegalArgumentException{
    	//Trying to load pictures via URL, thows exeption if unable 
    	String url = pic2url.getText();
    	try {
        	img3.setImage(new Image(url));

		} catch (Exception e) {
			img3.setImage(new Image(defaulturl));
			pic3url.setText("Error: Invalid picture url");
			errorfield.setText("Error: Invalid picture url");

		}
    }
    
    @FXML
    void upload4(ActionEvent event)  throws IllegalArgumentException{
    	//Trying to load pictures via URL, thows exeption if unable 
    	String url = pic2url.getText();
    	try {
        	img4.setImage(new Image(url));

		} catch (Exception e) {
			img3.setImage(new Image(defaulturl));
			pic3url.setText("Error: Invalid picture url");
			errorfield.setText("Error: Invalid picture url");

		}
    }
    
    @FXML
    void generate(ActionEvent event) throws IOException
    {
    	//loads all the pictures 
    	Image image1 = img1.getImage();
		Image image2 = img2.getImage();
		Image image3 = img3.getImage();
		Image image4 = img4.getImage();
		
		//load border size
    	border = Integer.parseInt(borderbreddefelt.getText());

		/*Image logo = null;
		try {
			 logo = new Image("logo.jpg");

		} catch (Exception e) {
			errorfield.setText("finner ikke logo");
		}*/
		//System.out.println("hei"+logo.getHeight());
		/*Integer largesth = (int) image1.getHeight();
		if (image2.getHeight()>largesth) largesth = (int) image2.getHeight();
		if (image3.getHeight()>largesth) largesth = (int) image3.getHeight();
		
		Integer largestw = (int) image3.getHeight();
		if (largestw> image1.getHeight()+image2.getHeight()) largestw =(int) (image1.getHeight()+image2.getHeight());
		
		Integer width = largestw;

		Integer height = largesth;
		*/
		
    	
    	//combines the with of the pictures to remove outofbounds problems
    	// finds the largest possible width 
		Integer width = (int) (image1.getWidth()+image2.getWidth());
    	if (image3.getWidth()+image4.getWidth()>width) width = (int) ((int) image3.getWidth()+image4.getWidth());

    	//combines the height of the pictures to remove out of bounds problem
    	// finds the largetst possible height 
		Integer height = (int) ((int) image1.getHeight()+image3.getHeight());
		if (image2.getHeight()+image4.getHeight()>height) height = (int) ( image2.getHeight()+image4.getHeight());
		WritableImage writableImage = new WritableImage(width+border, height+border);
		//System.out.println("ww"+writableImage.getWidth());
		//System.out.println("wh"+writableImage.getHeight());

		//Image white = new Image(width+border,height+border);
      
		PixelReader pixelReader1 = null;
		PixelReader pixelReader2 = null ;
		PixelReader pixelReader3 = null ;
		PixelReader pixelReader4 = null ;

		
		try {
			 pixelReader1 = image1.getPixelReader();
			 pixelReader2 = image2.getPixelReader();
			 pixelReader3 = image3.getPixelReader();
			 pixelReader4 = image4.getPixelReader();

		} catch (Exception e) {
			errorfield.setText("Pixelreader error");
		}
		

		//PixelReader whitereader = white.getPixelReader();
		
		//System.out.println(width);
		//System.out.println(height);

		
		//Integer logow= (int) ((image1.getHeight()+image2.getHeight())-image3.getHeight());
		//Integer logoh = (int) image3.getWidth();

		BufferedImage bf;
		
		//RenderedImage rend = SwingFXUtils.fromFXImage(logo, null);
		//bf =(BufferedImage) rend;
		
		/*BufferedImage re1 = new BufferedImage(logoh, logow, bf.getType());
		Graphics2D gra = re1.createGraphics();
		gra.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
		    RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		gra.drawImage(bf, 0, 0, logoh, logow, 0, 0, bf.getWidth(),
				bf.getHeight(), null);
		gra.dispose();
		*/
		Image convertedlogo; 
		
		//PixelReader pixelReader4 = null;
		
		PixelWriter pixelWriter = writableImage.getPixelWriter();
		/*try {

			logo = SwingFXUtils.toFXImage(re1, null);;
			
			 pixelReader4 = logo.getPixelReader();

			 pixelWriter = writableImage.getPixelWriter();
		} catch (Exception e) {
			errorfield.setText("logo pixelreader error");
		} */
			
		
		
		/*
		 * First the program generates a white background to work with, its the size of tallest and widest size
		 * then it goes trough all 4 pictures one by one, setting them appart depending on the border size
		 */

		for (int y = 0; y < writableImage.getHeight(); y++){
		    for (int x = 0; x < writableImage.getWidth(); x++){
		        Color color = new Color(1, 1, 1, 1);
		        
		        pixelWriter.setColor(x, y, color);
		    }
		}		
        System.out.println("The white backgorund has been created");


		for (int y = 0; y < image1.getHeight(); y++){
		    for (int x = 0; x < image1.getWidth(); x++){
		        Color color = pixelReader1.getColor(x, y);
		        pixelWriter.setColor(x, y, color);
		    }
		}
        System.out.println("1 done");

  	
		
		for (int y = 0; y < image2.getHeight(); y++){
			for(int x = 0; x< image2.getWidth(); x++)
			{
				Color color = pixelReader2.getColor(x, y);
		        pixelWriter.setColor((int) (x + image1.getWidth()+border),y,color);

			}
		
		}
        System.out.println("2 done");

		for (int y = 0; y < image3.getHeight(); y++){
			for(int x = 0; x< image3.getWidth(); x++)
			{
				Color color = pixelReader3.getColor(x, y);
		        pixelWriter.setColor(x, (int) (y+image1.getHeight()+border), color);

			}
		
		}
  	
        System.out.println("3 done");

		for (int y = 0; y < image4.getHeight(); y++){
			for(int x = 0; x< image4.getWidth(); x++)
			{
				Color color = pixelReader4.getColor(x, y);
		        pixelWriter.setColor((int) (x+image1.getWidth()+border), (int) (y+image1.getHeight()+border), color);

			}
		
		}
        System.out.println("4 done");

		/*for (int y = 0; y < re1.getHeight(); y++){
			for(int x = 0; x< re1.getWidth(); x++)
			{
				Color color = pixelReader4.getColor(x, y);
		        pixelWriter.setColor((int) (x+image1.getWidth()), (int) (y+image3.getHeight()), color);

			}
		
		}*/
		 

		// try to create a file 
		try {
			
			File file = null; 
			File file2 = null; 
			//check for duplicate names, if test or name exist, add a random number to filename 
			if (filename != "test")
			{
				file =new File(filename+".jpg");
				if (file.exists())
				{
					file2 =new File(filename+System.currentTimeMillis() % 10000+ ".jpg");
					file.renameTo(file2);
				}

			}
			else
			{
				file =new File(filename+System.currentTimeMillis() % 10000+ ".jpg");

			}
			BufferedImage buffimage = new BufferedImage(width, height,BufferedImage.TYPE_INT_RGB);
			RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
			buffimage =(BufferedImage) renderedImage;
			
			dropAlphaChannel(buffimage); //to make it generate a jpg and not a png
			
			
			BufferedImage resized = new BufferedImage(4252, 2835, buffimage.getType());
			Graphics2D graRez = resized.createGraphics();
			graRez.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
			    RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			graRez.drawImage(buffimage, 0, 0, 4252, 2835, 0, 0, buffimage.getWidth(), buffimage.getHeight(), null);
			//hardcoded variables becasue client asked for it
			//change variable 3 and 4 for the size you want 
			graRez.dispose();
			
			ImageIO.write( dropAlphaChannel( resized), "jpg",file);
			if(file.exists()) errorfield.setText("Picture sucessfuly generated");
			else errorfield.setText("Unable to generate picture");
		
			
		} catch (Exception e) {
			errorfield.setText("Error in filegeneration");
		}
		

		
		
		genimg.setImage(writableImage);
    	
    }
    
    
    //jpg does not like aplha 
    public BufferedImage dropAlphaChannel(BufferedImage src) {
        BufferedImage convertedImg = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_INT_RGB);
        convertedImg.getGraphics().drawImage(src, 0, 0, null);

        return convertedImg;
   }
    
    @FXML
    void prefmenu(ActionEvent event) {

    	TextInputDialog dialog = new TextInputDialog(filename);
    	dialog.setContentText("Please enter filename");

    	// menu for filename 
    	Optional<String> result = dialog.showAndWait();
    	if (result.isPresent()){
    		filename = result.get();
    		
    	}

    	
    	
    }
    
    @FXML
    void filetypemenu(ActionEvent event) {

    	//filetype menu, removed for now becasue the client only wanted jpg
    	TextInputDialog dialog = new TextInputDialog(filetype);
    	dialog.setContentText("Please enter filetype");

    	// Traditional way to get the response value.
    	Optional<String> result = dialog.showAndWait();
    	if (result.isPresent()){
    		filetype = result.get();
    		System.out.println(filetype);
    	}

    	
    	
    }
    
    
    @FXML
    void aboutmenu(ActionEvent event) {
    	
    	Alert alert = new Alert(AlertType.INFORMATION);
    	alert.setTitle("Information Dialog");
    	alert.setHeaderText(null);
    	alert.setContentText(""
    			+ "known bugs and functions:"
    			+ "\npicture is saved where the program is saved");

    	alert.showAndWait();

    }
    
    @FXML
    private void handleDragover(DragEvent event)
    
    {	//handle drag over events
    	if (event.getDragboard().hasFiles()) {
    		event.acceptTransferModes(TransferMode.ANY);
		}
    }
    
    //handle drops for all picture input fields 
    
    @FXML
    private void handledrop1(DragEvent event) throws FileNotFoundException
    {
    	List <File> file =  event.getDragboard().getFiles();
    	Image img = new Image(new FileInputStream(((List<File>) file).get(0)));
    	img1.setImage(img);
    }
    @FXML
    private void handledrop2(DragEvent event) throws FileNotFoundException
    {
    	List <File> file =  event.getDragboard().getFiles();
    	Image img = new Image(new FileInputStream(((List<File>) file).get(0)));
    	img2.setImage(img);
    }
    @FXML
    private void handledrop3(DragEvent event) throws FileNotFoundException
    {
    	List <File> file =  event.getDragboard().getFiles();
    	Image img = new Image(new FileInputStream(((List<File>) file).get(0)));
    	img3.setImage(img);
    }
    
    @FXML
    private void handledrop4(DragEvent event) throws FileNotFoundException
    {
    	List <File> file =  event.getDragboard().getFiles();
    	Image img = new Image(new FileInputStream(((List<File>) file).get(0)));
    	img4.setImage(img);
    }
    
    @FXML
    private void setBoarderbredde() 
    { 
    	System.out.println(borderbreddefelt.getText());
    	border = Integer.parseInt(borderbreddefelt.getText());
    }
    
    
    //same as generate function without the save to file part 
    @FXML
    private void preview(ActionEvent event)
    {
    	Image image1 = img1.getImage();
    	Image image2 = img2.getImage();
    	Image image3 = img3.getImage();
    	Image image4 = img4.getImage();

    	Image customimg = null; 
		
    	Integer largest = (int) image1.getWidth();
    	if (image2.getHeight()>largest) largest = (int) image2.getHeight();
    	if (image3.getHeight()>largest) largest = (int) image3.getHeight();
    	if (image4.getHeight()>largest) largest = (int) image4.getHeight();

		//Integer height = (int) (image1.getHeight()+image3.getHeight()+border*2);
		//Integer width = (int) (image1.getWidth()+image2.getWidth()+border*2);
		
    	Integer larger = (int) (image1.getHeight()+image3.getHeight());
    	if (image2.getHeight()+image4.getHeight()>larger) larger = (int) (image2.getHeight()+image4.getHeight());

		Integer height = larger;
		Integer width = (int) (image1.getWidth()+image2.getWidth());
    	if (image3.getWidth()+image4.getWidth()>width) width = (int) (image3.getWidth()+image4.getWidth());

		
		PixelReader pixelReader1 = image1.getPixelReader();
		PixelReader pixelReader2 = image2.getPixelReader();
		PixelReader pixelReader3 = image3.getPixelReader();
		PixelReader pixelReader4 = image4.getPixelReader();

		WritableImage writableImage = new WritableImage(width, height);
		
		
		PixelWriter pixelWriter = writableImage.getPixelWriter();
		
			
		for (int y = 0; y < image1.getHeight(); y++){
            for (int x = 0; x < image1.getWidth(); x++){
                Color color = pixelReader1.getColor(x, y);
                pixelWriter.setColor(x, y, color);
            }
        }
		
	
		
		for (int y = 0; y < image2.getHeight(); y++){
			for(int x = 0; x< image2.getWidth(); x++)
			{
				Color color = pixelReader2.getColor(x, y);
                pixelWriter.setColor((int) (x+image1.getWidth()), y, color);

			}
		
		}
		for (int y = 0; y < image3.getHeight(); y++){
			for(int x = 0; x< image3.getWidth(); x++)
			{
				Color color = pixelReader3.getColor(x, y);
                pixelWriter.setColor( x, (int) (y+image1.getHeight()), color);

			}
		
		}
		for (int y = 0; y < image4.getHeight(); y++){
			for(int x = 0; x< image4.getWidth(); x++)
			{
				Color color = pixelReader4.getColor(x, y);
                pixelWriter.setColor((int) (x+image1.getWidth()), (int) (y+image1.getHeight()), color);

			}
		
		}
		
		
		genimg.setImage(writableImage);

    }
    
}
